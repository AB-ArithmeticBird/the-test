name := """the-test"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala).settings(
  watchSources ++= (baseDirectory.value / "public/ui" ** "*").get
)

resolvers += Resolver.sonatypeRepo("snapshots")

scalaVersion := "2.12.8"

libraryDependencies += guice
libraryDependencies += "org.scalatestplus.play" %% "scalatestplus-play" % "4.0.2" % Test
libraryDependencies += "com.h2database" % "h2" % "1.4.199"
libraryDependencies +="org.scalikejdbc" %% "scalikejdbc"       % "3.2.1"
libraryDependencies +="org.scalikejdbc" %% "scalikejdbc-config"  % "3.2.1"
libraryDependencies +="ch.qos.logback" % "logback-classic" % "1.2.3"
libraryDependencies +="ch.qos.logback" % "logback-classic" % "1.2.3" % Test
libraryDependencies +="org.postgresql" % "postgresql" % "9.4-1200-jdbc41" exclude("org.slf4j", "slf4j-simple")
libraryDependencies +="org.julienrf" %% "play-json-derived-codecs" % "6.0.0"