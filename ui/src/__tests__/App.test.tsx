import React from 'react';
import ReactDOM from 'react-dom';

import axios from "axios";
import App from "../App";

test("should render the app ", async () => {
  const mock = jest.spyOn(axios, 'get');

  mock.mockReturnValueOnce( new Promise<number>((resolve, reject) => {
    setTimeout( () => {
      1
    }, 5000);
  }));
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});
