import React from "react";
import { render} from "@testing-library/react";
import {Main, MainProps} from "../components/Main";
import axios from 'axios';


describe("<Main/>", () => {

  test("should display a main ", async () => {
    const { findByTestId } = renderMainForm();
    const mock = jest.spyOn(axios, 'get');

    mock.mockReturnValueOnce( new Promise<number>((resolve, reject) => {
      setTimeout( () => {
       1
      }, 5000);
    }));
    
    const mf = await findByTestId('main');
    expect(mf).toHaveClass('container');
  });
});




function renderMainForm(props: Partial<MainProps> = {}){
  const defaultProps:MainProps = {};
  return render(<Main {...defaultProps} {...props}/> )
}
