import React, {Props} from "react";
import { render, fireEvent} from "@testing-library/react";
import {ProfileFormComponent, ProfileFormProps} from "../components/ProfileFormComponent";
import {Profile} from "../interfaces/Profile";
import {Ethnicity, Figure, Gender, Religion} from "../enums/enum";


describe("<ProfileForm/>", () => {
  test("should display a profile form ", async () => {
    const { findByTestId } = renderProfileForm();

    const pf: any = await findByTestId('profile-form');

    expect(pf).toHaveFormValues({
      displayName: "ahmad",
      realName: 'akbar ahmad',
    });
  });
});

test("should allow editing the profile", async () => {
  const buttonClick = jest.fn();
  const { findByTestId } = renderProfileForm({ buttonClick });

  const edit = await findByTestId("edit");
  
  fireEvent.click(edit);
  expect(buttonClick).toHaveBeenCalledWith({"aboutMe": "alpha beta gamma", "birthdate": "08/01/1988", "displayName": "ahmad", "ethnicity": 10, "figure": 1, "gender": 0, "height": 0, "id": "123", "location": {"name": "saefdew", "value": {"lat": "1", "lon": "2"}}, "realName": "akbar ahmad", "religion": 9});
});



function renderProfileForm(props: Partial<ProfileFormProps> = {}){
  const defaultProps:ProfileFormProps = {
     profile :  {
       id: '123',
       displayName: 'ahmad',
       realName: 'akbar ahmad',
       birthdate: '08/01/1988',
       gender: Gender.Male,
       ethnicity: Ethnicity.Other,
       religion: Religion.Other,
       height: 0,
       figure: Figure.Normal,
       aboutMe: 'alpha beta gamma',
       location: {name:'saefdew', value:{lat:'1',lon:'2'}}
     },
      buttonClick:function (p:Profile){
          return;
      },
      buttonType: 'edit',
      cancelClick:function (){return;}
  };
  return render(<ProfileFormComponent {...defaultProps} {...props}/> )
}
