import {Ethnicity, Figure, Gender, Religion} from "../enums/enum";

export interface Value{
  lat: string;
  lon: string;
}

export interface Location {
  name:string;
  value:Value;
}
export interface Profile {
    id: string;
    displayName: string;
    realName:string;
    //profile_picture?: boolean;
    birthdate: string;
    gender:Gender; //Enum
    ethnicity?: Ethnicity; //Enum
    religion?: Religion; //Enum
    height?: number;
    figure?: Figure;
    aboutMe?:string;
    location:Location;
}
