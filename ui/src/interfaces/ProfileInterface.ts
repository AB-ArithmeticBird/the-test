import {Profile} from "./Profile";

export interface ProfilesInterface {
    profiles: Profile[];
}
export interface ProfileInterface {
    profile:Profile;
}