import {Profile} from "./Profile";

export type ProfileFormButtonType = 'edit' | 'create';

export interface EditCreateButtonClicks {
    editClick: (profile: Profile) => void;
}