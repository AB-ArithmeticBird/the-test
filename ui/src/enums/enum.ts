// define enum
export enum Gender {
    Male,
    Female,
    Other
}

export enum Ethnicity {
    White,
    SouthAsian,
    SouthEastAsian,
    Mixed,
    Black,
    Arabic,
    Hispanic ,
    Latino,
    NativeAmerican,
    PacificIslander,
    Other
}

export enum Religion {
    Agnostic,
    Atheist,
    Buddhist,
    Christian,
    Hindu,
    Islam,
    Jewish,
    Shinto,
    Sikh,
    Other
}

export enum Figure {
    Slim,
    Normal,
    Athletic,
    AFewExtraKilos,
    MoreToLove
}

export enum MaritalStatus {
    NeverMarried,
    Divorced,
    Widower,
    Separated
}



const StringIsNumber = (value:any) => !isNaN(Number(value));

  // Turn enum into array
  export function ToArray(enumme:any) {
    let map: {label: string; value: number}[] = [];
    Object.keys(enumme).filter(StringIsNumber).forEach (n =>{
        map.push({value: n as any, label: enumme[n] as any});
    });
    return map;
  }

