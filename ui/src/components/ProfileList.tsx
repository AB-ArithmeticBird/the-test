import React from "react";
import ProfileListItem from "./ProfileListItem";
import {EditCreateButtonClicks} from "../interfaces/EditButtonClicks";
import {ProfilesInterface} from "../interfaces/ProfileInterface"

const ProfileList = (props: ProfilesInterface & EditCreateButtonClicks) => {
    return (
        <table className="table table-hover table-striped table-bordered">
            <thead>
            <tr>
                <th>DisplayName</th>
                <th>BirthDay</th>
                <th>Id</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            {props.profiles && props.profiles.map(profile =>
                <ProfileListItem profile={profile}
                        key={profile.id}
                        editClick={props.editClick}/> )}
            </tbody>
        </table>
    );
};
export default ProfileList;
