import Autosuggest from 'react-autosuggest';
import * as React from 'react';
import axios from 'axios';

export interface CityState {
  cities: City[];
  currentCity:City
}
export interface City {
  name: string;
  value: { lat:string,lon:string};
}

export interface CityProps {
  id:string
  currentCity: City
  onSuggestionSelected : (data:Autosuggest.SuggestionSelectedEventData<City>) => void
}

function escapeRegexCharacters(str: string): string {
  return str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
}

const CityAutosuggest = Autosuggest as unknown as { new (): Autosuggest<City> };

export class CityAutosuggestTyped extends React.Component<CityProps, CityState> {


  constructor(props: CityProps) {
    super(props);
    this.onSuggestionsSelected =  this.onSuggestionsSelected.bind(this);
    this.state = {
      cities: [],
      currentCity: {
        name: props.currentCity.name,
        value: {lat: props.currentCity.value.lat, lon: props.currentCity.value.lon}
      }
    }
  }
  
  static cities: City[] = [];


  render(): JSX.Element {
    const {currentCity, cities} = this.state;

    const theme = {
      input: 'themed-input-class',
      container: 'themed-container-class',
      suggestionFocused: 'active',
      sectionTitle: { color: 'blue' }
    };
    let value:string;
    if( currentCity.name !=="") {
      value = currentCity.name;
    }else {
      value = "";
    }

    return <CityAutosuggest
      suggestions={cities}
      onSuggestionsFetchRequested={this
        .onSuggestionsFetchRequested
        .bind(this)}
      getSuggestionValue={this.getSuggestionValue}
      renderSuggestion={this.renderSuggestion}
      onSuggestionSelected={this.onSuggestionsSelected}
      alwaysRenderSuggestions={true}
      inputProps={{
        placeholder: 'type city',
        value,
        onChange: (e, changeEvent) => this.onChange(e, changeEvent),
      }}
      theme={theme}/>;
  }

  protected onSuggestionsSelected(event: React.FormEvent<any>, data: Autosuggest.SuggestionSelectedEventData<City>): void {
    this.props.onSuggestionSelected(data);
  }

  protected renderSuggestion(suggestion: City, params: Autosuggest.RenderSuggestionParams): JSX.Element {
    const className = params.isHighlighted ? "highlighted" : undefined;
    return <span className={className}>{suggestion.name}  ({suggestion.value.lat},{suggestion.value.lat})</span>
  }

  protected onChange(event: React.FormEvent<any>, {newValue, method}: Autosuggest.ChangeEvent): void {
    let newCity = {name:newValue, value:{lat:"",lon:""}};
    this.setState({currentCity: newCity});
  }

  protected async onSuggestionsFetchRequested({value}: any) {
     await this.getSuggestions(value);
  }

  protected async getSuggestions(value: string="London") {


    const escapedValue = escapeRegexCharacters(value.trim());

    if (escapedValue === '') {
      return [];
    }
    
    
    let cities = await axios.get('/api/city/'+ escapedValue);

    let c: City[] =  cities.data.cities.map(function (d: any) {
      let c:City = ({name: d.city, value: {lat: d.lat, lon: d.lon}});
      return c;
    });

    this.setState({cities:c});

  }

  protected getSuggestionValue(suggestion: City): string { return suggestion.name; }

}
