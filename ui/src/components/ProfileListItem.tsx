import React from "react";
import {Profile} from "../interfaces/Profile";
import {EditCreateButtonClicks} from "../interfaces/EditButtonClicks";
interface Props {
    profile: Profile;
}

const ProfileListItem = (props: Props & EditCreateButtonClicks) => {
    return <tr>
        <td>{props.profile.displayName}</td>
        <td>{props.profile.birthdate}</td>
        <td>{props.profile.id}</td>
        <td>
            <div className="row">
                <div className="col-md-3">
                    <button className='btn btn-warning'
                            onClick={() => props.editClick(props.profile)}>
                        Edit
                    </button>
                </div>
            </div>
        </td>
    </tr>
};
export default ProfileListItem;
