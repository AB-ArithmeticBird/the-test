import * as React from "react";
import {Profile} from "../interfaces/Profile";
import {ProfileFormButtonType} from "../interfaces/EditButtonClicks";
import {ProfilesInterface} from "../interfaces/ProfileInterface";
import axios from 'axios';
import {ProfileFormComponent} from "./ProfileFormComponent"
import ProfileList from "./ProfileList";
import {Ethnicity, Figure, Gender, Religion} from "../enums/enum";


export interface MainState {
    showProfileForm: boolean;
    profileFormProfile?: Profile;
    profileFormButtonType: ProfileFormButtonType
}
export interface MainProps {

}

export class Main extends React.Component<MainProps, MainState & ProfilesInterface> {
    constructor(props:MainProps){
        super(props);
        this.state = {
            profiles : ([] as Profile[]),
            showProfileForm : false,
            profileFormButtonType : 'edit'
        };

        this.editClick = this.editClick.bind(this);
        this.formButtonClick = this.formButtonClick.bind(this);
        this.personFormCancelClick = this.personFormCancelClick.bind(this);
    }

    editClick(profile: Profile) {
        this.setState({showProfileForm: true, profileFormProfile: profile, profileFormButtonType: 'edit'});
    }

    async formButtonClick(profile: Profile) {
        let func = this.state.profileFormButtonType === 'edit' ? axios.put : axios.post;
        await func('/api/profiles', profile);
        await this.loadProfiles();
        this.setState({showProfileForm: false});
    }

    async componentDidMount() {
        this.loadProfiles();
    }
    
    async loadProfiles(){
        let profiles = await axios.get('/api/profiles');
        let ps =  profiles.data;
        let pros = ps.map((p:any) => {
          let r, g, f, e;
          if (p.religion) {
           r = (Religion as { [key: string]: any})[Object.keys(p.religion)[0]];
          } else r = undefined ;

          if (p.ethnicity) {
            e =  (Ethnicity as { [key: string]: any })[Object.keys(p.ethnicity)[0]];
          } else e = undefined ;

          if (p.gender) {
            g =  (Gender  as { [key: string]: any })[Object.keys(p.gender)[0]];
          } else g = undefined ;

          if (p.figure) {
            f =  (Figure  as { [key: string]: any })[Object.keys(p.figure)[0]];
          } else f = undefined ;

          let newVar:Profile = {
            id: p.id,
            displayName: p.displayName,
            realName: p.realName,
            birthdate: p.birthdate,
            gender: g,
            ethnicity: e,
            religion: r,
            height: p.height,
            figure: f,
            aboutMe: p.aboutMe,
            location: p.location
          };
          return newVar;
         }
        );
        this.setState({profiles: pros});
    }



    personFormCancelClick() {
        this.setState({showProfileForm: false});
    }

    render() {
        return(
            <div className='container'  data-testid="main">
                <h1>My Profiles App</h1>

                {this.state.showProfileForm && <ProfileFormComponent
                    profile={this.state.profileFormProfile}
                    buttonType={this.state.profileFormButtonType}
                    buttonClick={this.formButtonClick}
                    cancelClick={this.personFormCancelClick} />}
                <ProfileList profiles={this.state.profiles}
                        editClick={this.editClick} />
            </div>
        );
    }
}
