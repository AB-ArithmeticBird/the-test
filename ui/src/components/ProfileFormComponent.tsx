import * as React from "react";
import {Profile} from "../interfaces/Profile";
import {ProfileFormButtonType} from "../interfaces/EditButtonClicks";
import {ProfileInterface} from "../interfaces/ProfileInterface";
import {Ethnicity, Figure, Gender, Religion, ToArray} from "../enums/enum";
import Select from 'react-select';
import {City, CityAutosuggestTyped} from "./CityAutoSuggesTyped";
import Autosuggest from 'react-autosuggest';

export interface ProfileFormProps {
  profile?: Profile;
  buttonClick: (profile: Profile) => void;
  buttonType: ProfileFormButtonType;
  cancelClick: () => void;
}


export class ProfileFormComponent extends React.Component<ProfileFormProps, ProfileInterface> {

  constructor(props: ProfileFormProps) {

    super(props);

    this.onTextChange = this.onTextChange.bind(this);
    this.buttonClick = this.buttonClick.bind(this);
    this.onSuggestionsSelected = this.onSuggestionsSelected.bind(this);
    this.state = {
      profile: props.profile ? {...props.profile}
        : {
          id: '',
          displayName: '',
          realName: '',
          birthdate: '',
          gender: Gender.Other,
          ethnicity: Ethnicity.Other,
          religion: Religion.Other,
          height: 0,
          figure: Figure.Normal,
          aboutMe: '',
          location: {name:'', value:{lat:'',lon:''}}
        }
    }
  }


  onTextChange(e: any) {
    let profile: any = this.state.profile;
    if (e.target.name==='height') {
      profile[e.target.name] = (e.target.validity.valid) ? +e.target.value : profile[e.target.name];
    }else{
      profile[e.target.name] = (e.target.validity.valid) ? e.target.value : profile[e.target.name];
    }
    this.setState(profile);
  }

  handleChange = (evt: any, selectedOption: any) => {
    let profile: any = this.state.profile;
    profile[selectedOption.name] = +evt.value;
    this.setState({profile});
  };


  buttonClick(evt: React.MouseEvent<HTMLButtonElement>) {
    evt.preventDefault();
    this.props.buttonClick(this.state.profile);
  }

  componentWillReceiveProps(props: ProfileFormProps) {
    this.setState({
      profile: props.profile ? {...props.profile}
        : {
          id: '',
          displayName: '',
          realName: '',
          birthdate: '',
          gender: Gender.Other,
          ethnicity: Ethnicity.Other,
          religion: Religion.Other,
          height: 0,
          figure: Figure.Normal,
          aboutMe: '',
          location: {name:'', value:{lat:'',lon:''}}
        }
    })
  }


  onSuggestionsSelected(data: Autosuggest.SuggestionSelectedEventData<City>): void {
    let profile: any = this.state.profile;
    profile['location'] = data.suggestion;
    this.setState({profile});
  }


  render() {
    let r, e, g, f;

    let rel: Religion | undefined = this.state.profile.religion;
    
    if (rel !== undefined) {
      r = {label: Religion[rel], value: rel};
    } else r = undefined;


    let eth: Ethnicity | undefined = this.state.profile.ethnicity;
    
    if (eth !== undefined) {
      e = {label: Ethnicity[eth], value: eth};
    } else e=undefined;

    let gen: Gender = this.state.profile.gender;
    g = {label: Gender[gen], value: gen};


    let fig: Figure | undefined = this.state.profile.figure;
    if (fig !== undefined) {
      f = {label: Figure[fig], value: fig};
    } else f = undefined;

    let curr = {name: this.state.profile.location.name, value:{lat: this.state.profile.location.value.lat,lon:this.state.profile.location.value.lon}};

    return (

      <form data-testid='profile-form'>
        <label htmlFor="display-name">Display Name</label>
        <input
          id="display-name"
          data-testid='display-name'
          className='form-control'
          name='displayName'
          onChange={e => this.onTextChange(e)}
          type='text'
          value={this.state.profile.displayName}
          placeholder="DisplayName"

        />

        <label htmlFor="birth-date">Birth Date</label>
        <input
          id="birth-date"
          className='form-control'
          name='birthdate'
          onChange={e => this.onTextChange(e)}
          type='text'
          value={this.state.profile.birthdate}
          placeholder="Birthday"
        />

        <label htmlFor="real-name">Real Name</label>
        <input
          id="real-name"
          className='form-control'
          name='realName'
          onChange={e => this.onTextChange(e)}
          type='text'
          value={this.state.profile.realName}
          placeholder="RealName"
        />

        <label htmlFor="gender">Gender</label>
        <Select
          id="gender"
          name="gender"
          value={g}
          options={ToArray(Gender)}
          onChange={this.handleChange}
          placeholder="Please Select the gender"
        />

        <label htmlFor="ethnicity">Ethnicity</label>
        <Select
          id="ethnicity"
          name="ethnicity"
          value={e}
          options={ToArray(Ethnicity)}
          onChange={this.handleChange}
          placeholder="Please Select the Ethnicity"
        />

        <label htmlFor="religion">Religion</label>
        <Select
          name="religion"
          value={r}
          options={ToArray(Religion)}
          onChange={this.handleChange}
          placeholder="Please Select the Religion"
        />

        <label htmlFor="figure">Figure</label>
        <Select
          id="figure"
          name="figure"
          value={f}
          options={ToArray(Figure)}
          onChange={this.handleChange}
          placeholder="Please Select the Figure"
        />

        <label htmlFor="height">Height</label>
        <input
          id="height"
          className='form-control'
          name='height'
          onChange={e => this.onTextChange(e)}
          type='number'
          value={this.state.profile.height}
          pattern="[0-9]*"
          placeholder="Height"
        />

        <label htmlFor="aboutMe">aboutMe</label>
        <input
          id="aboutMe"
          className='form-control'
          name='aboutMe'
          onChange={e => this.onTextChange(e)}
          type='text'
          value={this.state.profile.aboutMe}
          placeholder="Aboutme"
        />
        
        <label htmlFor="location">Location</label>
        <CityAutosuggestTyped id="location" currentCity={curr} onSuggestionSelected={this.onSuggestionsSelected}/>
        <button
          data-testid="edit"
          className={this.props.buttonType === 'edit' ? 'btn btn-success' : 'btn btn-primary'}
          onClick={this.buttonClick}
        >
          {this.props.buttonType === 'edit' ? 'Update' : 'Add'}
        </button>
        <button type='button'
                className='btn btn-danger'
                onClick={this.props.cancelClick}
        >
          Cancel
        </button>
      </form>
    )
  }

}
