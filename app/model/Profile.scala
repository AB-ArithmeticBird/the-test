package model

import julienrf.json.derived
import play.api.libs.json.{Json, OFormat}

sealed trait Gender {
  def id:Int
}

case object Male extends Gender {
  def id = 0
}

case object Female extends Gender {
  def id = 1
}

case object Others extends Gender {
  def id = 2
}

object Gender{
  def fromInt(i:Int): Gender =  i match {
    case 0 => Male
    case 1 => Female
    case 2 => Others
  }
}
sealed trait Ethnicity {
  def id:Int
}

case object White extends Ethnicity {
  def id = 0
}

case object SouthAsian extends Ethnicity {
  def id = 1
}

case object SouthEastAsian extends Ethnicity {
  def id = 2
}

case object Mixed extends Ethnicity {
  def id = 3
}

case object Black extends Ethnicity {
  def id = 4
}

case object Arabic extends Ethnicity {
  def id = 5
}

case object Hispanic extends Ethnicity {
  def id = 6
}

case object Latino extends Ethnicity {
  def id = 7
}

case object NativeAmerican extends Ethnicity {
  def id = 8
}

case object PacificIslander extends Ethnicity {
  def id = 9
}

case object OtherEthnicity extends Ethnicity {
  def id = 10
}
object Ethnicity{
  def fromInt(i:Int): Ethnicity =  i match {
    case 0 => White
    case 1 => SouthAsian
    case 2 => Mixed
    case 3 => Black
    case 4 => Arabic
    case 5 => Hispanic
    case 6 => Latino
    case 7 => NativeAmerican
    case 8 => PacificIslander
    case _ => OtherEthnicity
  }
}

sealed trait Religion    {
  def id:Int
}

case object Agnostic extends Religion {
  def id = 0
}

case object Atheist extends Religion {
  def id = 1
}

case object Buddhist extends Religion {
  def id = 2
}

case object Christian extends Religion {
  def id = 3
}

case object Hindu extends Religion {
  def id = 4
}

case object Islam extends Religion {
  def id = 5
}

case object Jewish extends Religion {
  def id = 6
}

case object Shinto extends Religion {
  def id = 7
}

case object Sikh extends Religion {
  def id = 8
}

case object OtherReligion extends Religion {
  def id = 9
}

object Religion {
  def fromInt(i:Int): Religion =  i match {
    case 0 => Agnostic
    case 1 => Atheist
    case 2 => Buddhist
    case 3 => Christian
    case 4 => Hindu
    case 5 => Islam
    case 6 => Jewish
    case 7 => Shinto
    case 8 => Sikh
    case 9 => OtherReligion
  }
}


sealed trait Figure {
  def id:Int
}

case object Slim extends Figure {
  def id = 0
}

case object Normal extends Figure {
  def id = 1
}

case object Athletic extends Figure {
  def id = 2
}

case object AFewExtraKilos extends Figure {
  def id = 3
}

case object MoreToLove extends Figure {
  def id = 4
}

object Figure {
  def fromInt(i:Int): Figure =  i match {
    case 0 => Slim
    case 1 => Normal
    case 2 => Athletic
    case 3 => AFewExtraKilos
    case 4 => MoreToLove
  }
}


sealed trait MaritalStatus {
  def id:Int
}

case object NeverMarried extends MaritalStatus {
  def id = 0
}

case object Divorced extends MaritalStatus {
  def id = 1
}

case object Widower extends MaritalStatus {
  def id = 2
}

case object Separated extends MaritalStatus {
  def id = 3
}

 object MaritalStatus{
   def fromInt(i:Int): MaritalStatus =  i match {
     case 0 => NeverMarried
     case 1 => Divorced
     case 2 => Widower
     case 3 => Separated
   }
 }

case class Profile(id: String,
                   displayName: String,
                   realName: String,
                   birthdate: String,
                   gender: Gender,
                   ethnicity: Option[Ethnicity],
                   religion:Option[Religion],
                   height:Option[Int],
                   figure:Option[Figure],
                   maritalStatus: MaritalStatus,
                   occupation: Option[String],
                   aboutMe: Option[String],
                   location: String
                  )

object Formatter {
  
  implicit lazy val genderFormat:OFormat[Gender] = derived.oformat[Gender]()
  implicit lazy val ethnicityFormat: OFormat[Ethnicity] = derived.oformat[Ethnicity]()
  implicit lazy val religionFormat: OFormat[Religion] = derived.oformat[Religion]()
  implicit lazy val figureFormat: OFormat[Figure] = derived.oformat[Figure]()
  implicit lazy val maritalStatusFormat: OFormat[MaritalStatus] = derived.oformat[MaritalStatus]()

  implicit lazy val profileFormat: OFormat[Profile] = Json.format[Profile]

}

