package model

import play.api.libs.json.{Json, _}

case class Value(lat: String, lon: String)
case class Location(name: String, value: Value)
case class ProfileDto(
                       id: String,
                       displayName: String,
                       realName: String,
                       birthdate: String,
                       gender: Gender,
                       ethnicity: Option[Ethnicity] = None,
                       religion:Option[Religion] = None,
                       height:Option[Int] = None,
                       figure:Option[Figure]= None,
                       aboutMe: Option[String] = None,
                       location: Location
                     )

object ProfileDto {
  def fromProfile(profile: Profile): ProfileDto = {

    ProfileDto(
      id = profile.id,
      displayName = profile.displayName,
      realName = profile.realName,
      birthdate = profile.birthdate,
      gender = profile.gender,
      ethnicity = profile.ethnicity,
      religion = profile.religion,
      height = profile.height,
      figure = profile.figure,
      aboutMe = profile.aboutMe,
      location = Json.parse(profile.location).as[Location]
    )
  }


  implicit lazy val valueFormat: OFormat[Value] = Json.format[Value]
  implicit lazy val locationFormat: OFormat[Location] = Json.format[Location]
}