package model

import julienrf.json.derived
import play.api.libs.json.OFormat

case class City(lat:String, lon:String, city:String)

case class Cities(cities:Seq[City])
object CityFormatter {

  implicit lazy val cityFormat:OFormat[City] = derived.oformat[City]()
  implicit lazy val citiesFormat:OFormat[Cities] = derived.oformat[Cities]()

}