package controllers

import controllers.HomeController.Formatter._
import controllers.HomeController.{ProfileUpdateRequest, SuccessfulMessage}
import infra.MyExecutor
import javax.inject._
import julienrf.json.derived
import model._
import play.api.libs.json.{Json, _}
import play.api.mvc._
import service.{CityRepo, ProfileRepo}
import model.ProfileDto._
import scala.concurrent.ExecutionContext

@Singleton
class HomeController @Inject()(cc: ControllerComponents,
                               pRepo: ProfileRepo,
                               cRepo: CityRepo,
                              )(implicit exec: ExecutionContext) extends AbstractController(cc) {

  def appSummary: Action[AnyContent] = Action {
    Ok(Json.obj("content" -> "The Test!"))
  }

  def all: Action[AnyContent] =  Action.async { _ =>

    val res = pRepo.all(ExecutionContext.fromExecutor(MyExecutor.ioThreadPool))
    res.map{  r =>
      Ok(Json.toJson(r.map(HomeController.fromProfile)))
    }
  }

  import HomeController.Formatter._
  import play.api.libs.json._

  def update(): Action[JsValue] =  Action.async(parse.json) { request =>
    val profileFE = request.body.as[ProfileUpdateRequest]
    val res = pRepo.updateProfileById(profileFE.id, profileFE.toProfileDto)(ExecutionContext.fromExecutor(MyExecutor.ioThreadPool))
    res.map{  r =>
      Ok(Json.toJson(SuccessfulMessage("status updated", Option(profileFE.id))))
    }
  }

  def city (str: String): Action[AnyContent] = Action.async{
    import model.CityFormatter._
    cRepo.like(str).map(s=> Ok(Json.toJson(s)))
  }
}

object HomeController{


  
  case class ProfileUpdateRequest(
                      id: String,
                      displayName: String,
                      realName: String,
                      birthdate: String,
                      gender: Int,
                      ethnicity: Option[Int] = None,
                      religion:Option[Int] = None,
                      height:Option[Int] = None,
                      figure:Option[Int]= None,
                      aboutMe: Option[String] = None,
                      location: Location
                    ) {

    def toProfileDto: ProfileDto = {
      ProfileDto(
        this.id,
        this.displayName,
        this.realName,
        this.birthdate,
        Gender.fromInt(this.gender.toInt),
        this.ethnicity.map(x=>Ethnicity.fromInt(x)),
        this.religion.map(x => Religion.fromInt(x)),
        this.height.map(_.toInt),
        this.figure.map(x => Figure.fromInt(x)),
        this.aboutMe,
        this.location
      )
    }
  }

  
  case class ProfileDto(
                         id: String,
                         displayName: String,
                         realName: String,
                         birthdate: String,
                         gender: Gender,
                         ethnicity: Option[Ethnicity] = None,
                         religion:Option[Religion] = None,
                         height:Option[Int] = None,
                         figure:Option[Figure]= None,
                         aboutMe: Option[String] = None,
                         location: Location
                       )
    def fromProfile (profile:Profile): ProfileDto = {
      ProfileDto(
        id = profile.id,
        displayName = profile.displayName,
        realName = profile.realName,
        birthdate = profile.birthdate,
        gender = profile.gender,
        ethnicity = profile.ethnicity,
        religion = profile.religion,
        height = profile.height,
        figure = profile.figure,
        aboutMe = profile.aboutMe,
        location = Json.parse(profile.location).as[Location]
      )
    }



  sealed trait ClientMessage
  case class SuccessfulMessage(message: String, data: Option[String])
    extends ClientMessage
  case class ErrorMessage(message: String, details: Option[String])
    extends ClientMessage
  object Formatter {
    implicit lazy val sFormat: Format[SuccessfulMessage] =
      Json.format[SuccessfulMessage]
    implicit lazy val eFormat: Format[ErrorMessage] = Json.format[ErrorMessage]



    implicit lazy val genderFormat:OFormat[Gender] = derived.oformat[Gender]()
    implicit lazy val ethnicityFormat: OFormat[Ethnicity] = derived.oformat[Ethnicity]()
    implicit lazy val religionFormat: OFormat[Religion] = derived.oformat[Religion]()
    implicit lazy val figureFormat: OFormat[Figure] = derived.oformat[Figure]()
    
    implicit lazy val profileDtoFormat: OFormat[ProfileDto] = Json.format[ProfileDto]

    implicit lazy val profileFEFormat: OFormat[ProfileUpdateRequest] = Json.format[ProfileUpdateRequest]

  }
}