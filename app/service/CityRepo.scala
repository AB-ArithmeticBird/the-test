package service

import model.Cities

import scala.concurrent.{ExecutionContext, Future}

trait CityRepo {

  def all(implicit e: ExecutionContext):Future[Cities]

  def like(str: String)(implicit e: ExecutionContext):Future[Cities]
  
}