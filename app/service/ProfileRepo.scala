package service



import controllers.HomeController.ProfileDto
import model.Profile

import scala.concurrent.{ExecutionContext, Future}

trait ProfileRepo {

  def all(implicit e: ExecutionContext):Future[Seq[Profile]]

  def byId(id: String)(implicit e: ExecutionContext): Future[Option[Profile]]

  def updateProfileById(id: String, profile:ProfileDto)(
    implicit e: ExecutionContext): Future[Unit]

}
