package service

import model.{Cities, City}
import model.CityFormatter._
import play.api.libs.json.Json

import scala.concurrent.{ExecutionContext, Future}
import scala.io.Source

class CityRepoInterpreter extends CityRepo {
  override def all(implicit e: ExecutionContext): Future[Cities] = Future{
    val f = Source.fromFile("/Users/arithmeticbird/thetest/the-test/conf/city.json")
    val json = f.getLines.mkString
    val cities = Json.parse(json).as[Cities]
    f.close()
    cities
  }

  override def like(str: String)(implicit e: ExecutionContext): Future[Cities] = Future{
    val f = Source.fromFile("/Users/arithmeticbird/thetest/the-test/conf/city.json")
    val json = f.getLines.mkString
    val cities: Seq[City] = Json.parse(json).as[Cities].cities.filter(c => c.city.startsWith(str))
    f.close()
    Cities(cities)
  }
}