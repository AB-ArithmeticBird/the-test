package service

import java.util.UUID

import controllers.HomeController.ProfileDto
import model.{Ethnicity, Figure, Gender, MaritalStatus, Profile, Religion}
import play.api.libs.json.Json
import scalikejdbc._

import scala.concurrent.{ExecutionContext, Future}

class ProfileRepoInterpreter extends ProfileRepo{
  override def all(implicit e: ExecutionContext): Future[Seq[Profile]] = {
    Future {
      DB localTx { implicit session =>
        sql"select * from profile"
          .map(
            rs =>
              Profile(rs.string("id"),
                rs.string("displayName"),
                rs.string("realName"),
                rs.string("birthdate"),
                Gender.fromInt(rs.int("gender")),
                rs.intOpt("ethnicity").map(Ethnicity.fromInt),
                rs.intOpt("religion").map(Religion.fromInt),
                rs.intOpt("height"),
                rs.intOpt("figure").map(Figure.fromInt),
                MaritalStatus.fromInt(rs.int("maritalStatus")),
                rs.stringOpt("occupation"),
                rs.stringOpt("aboutMe"),
                rs.string("location")
              ))
          .list()
          .apply()
      }
    }
  }

  override def byId(id: String)(implicit e: ExecutionContext): Future[Option[Profile]] = {
    Future {
      DB localTx { implicit session =>
        sql"select * from filedata where id = ${UUID.fromString(id)}"
          .map(
            rs =>
              Profile(rs.string("id"),
                rs.string("displayName"),
                rs.string("realName"),
                rs.string("birthdate"),
                Gender.fromInt(rs.int("gender")),
                rs.intOpt("ethnicity").map(Ethnicity.fromInt),
                rs.intOpt("religion").map(Religion.fromInt),
                rs.intOpt("height"),
                rs.intOpt("figure").map(Figure.fromInt),
                MaritalStatus.fromInt(rs.int("maritalStatus")),
                rs.stringOpt("occupation"),
                rs.stringOpt("aboutMe"),
                rs.string("location")))
          .single
          .apply()
      }
    }
  }

  override def updateProfileById(id: String, profile: ProfileDto)(implicit e: ExecutionContext): Future[Unit] = {
     Future {
       import model.ProfileDto._
       val l = Json.toJson(profile.location).toString()
      DB localTx { implicit session =>
        sql"UPDATE profile SET displayName =  ${profile.displayName}, birthdate=${profile.birthdate}, realname=${profile.realName}, gender=${profile.gender.id}, ethnicity=${profile.ethnicity.map(_.id)}, religion=${profile.religion.map(_.id)}, height=${profile.height}, figure=${profile.figure.map(_.id)},  aboutMe=${profile.aboutMe}, location= ${l}    where id = ${UUID.fromString(id)}".update
          .apply()
      }
    }
  }
}
