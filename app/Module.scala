import com.google.inject.AbstractModule
import com.typesafe.config.ConfigFactory
import scalikejdbc.ConnectionPool
import service.{CityRepo, CityRepoInterpreter, ProfileRepo, ProfileRepoInterpreter}

/**
 * This class is a Guice module that tells Guice how to bind several
 * different types. This Guice module is created when the Play
 * application starts.

 * Play will automatically use any class called `Module` that is in
 * the root package. You can create modules in other locations by
 * adding `play.modules.enabled` settings to the `application.conf`
 * configuration file.
 */
class Module extends AbstractModule {

  override def configure() = {
    bind(classOf[ProfileRepo]).to(classOf[ProfileRepoInterpreter])
    bind(classOf[CityRepo]).to(classOf[CityRepoInterpreter])

    val c = ConfigFactory.load()
    ConnectionPool.singleton(c.getString("db.url"),
      c.getString("db.user"),
      c.getString("db.pass"))
  }

}
