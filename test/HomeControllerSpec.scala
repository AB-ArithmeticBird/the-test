package controllers

import model.{Cities, Male, NeverMarried, Profile}
import org.scalatestplus.play._
import org.scalatestplus.play.guice._
import play.api.test._
import play.api.test.Helpers._
import service.{CityRepo, ProfileRepo}

import scala.concurrent.{ExecutionContext, Future}



class HomeControllerSpec extends PlaySpec with GuiceOneAppPerTest with Injecting {
  import scala.concurrent.ExecutionContext.Implicits.global
  "HomeController GET" should {
                   import HomeControllerSpec._
    "render the appSummary resource from a new instance of controller" in {
      val controller = new HomeController(stubControllerComponents(), proRepo, cityRepo)
      val home = controller.appSummary().apply(FakeRequest(GET, "/summary"))

      status(home) mustBe OK
      contentType(home) mustBe Some("application/json")
      val resultJson = contentAsJson(home)
      resultJson.toString() mustBe """{"content":"The Test!"}"""
    }

    "render the appSummary resource from the application" in {
      val controller = inject[HomeController]
      val home = controller.appSummary().apply(FakeRequest(GET, "/summary"))

      status(home) mustBe OK
      contentType(home) mustBe Some("application/json")
      val resultJson = contentAsJson(home)
      resultJson.toString() mustBe """{"content":"The Test!"}"""
    }

    "render the appSummary resource from the router" in {
      val request = FakeRequest(GET, "/api/summary")
      val home = route(app, request).get

      status(home) mustBe OK
      contentType(home) mustBe Some("application/json")
      val resultJson = contentAsJson(home)
      resultJson.toString() mustBe """{"content":"The Test!"}"""
    }


    "render the profile resource from the router" in {
      val controller = new HomeController(stubControllerComponents(), proRepo, cityRepo)
      val request = FakeRequest(GET, "/api/profiles")
      val home = route(app, request).get

      status(home) mustBe OK
      contentType(home) mustBe Some("application/json")

    }

    "render the city resource from the router" in {
      val controller = new HomeController(stubControllerComponents(), proRepo, cityRepo)
      val request = FakeRequest(GET, "/api/city/Lon")
      val home = route(app, request).get

      status(home) mustBe OK
      contentType(home) mustBe Some("application/json")

    }
  }
}

object HomeControllerSpec{
  val proRepo = new ProfileRepo {
    override def all(implicit e: ExecutionContext): Future[Seq[Profile]] = Future.successful {
        Seq(Profile("1","Tin", "1/1/92","",Male, None,None,None,None, NeverMarried,None, None,""))
    }

    override def byId(id: String)(implicit e: ExecutionContext): Future[Option[Profile]] = Future.successful{
      None
    }

    override def updateProfileById(id: String, profile: HomeController.ProfileDto)(implicit e: ExecutionContext): Future[Unit] =
      Future.successful()
  }

  val cityRepo = new CityRepo {
    override def all(implicit e: ExecutionContext): Future[Cities] = Future(Cities(Seq()))

    override def like(str: String)(implicit e: ExecutionContext): Future[Cities] = Future(Cities(Seq()))
  }

}